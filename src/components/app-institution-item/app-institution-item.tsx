import {Component, Event, EventEmitter, h, Prop} from '@stencil/core';
import {popoverController} from "@ionic/core";

@Component({
  tag: 'app-institution-item',
  styleUrl: 'app-institution-item.css'
})
export class AppInstitutionItem {

  @Prop() institution: any;

  @Event() institutionEvent: EventEmitter;

  async onOptionsButtonClicked(event) {
    const popover = await popoverController.create({
      component: 'app-popover',
      componentProps: {
        buttons: [
          {text: 'Edit', color: 'primary', action: 'edit'},
          {text: 'Delete', color: 'danger', action: 'delete'}
        ]
      },
      showBackdrop: false,
      event
    });

    await popover.present();
    const {data} = await popover.onWillDismiss();
    if (!data) return;

    this.institutionEvent.emit({data, institution: this.institution})
  }

  render() {
    return [
      <ion-item lines="none">
        <div slot="start" class="color-circle-outer">
          <div class="color-circle-inner" style={{background: 'var(--ion-color-primary)'}}/>
        </div>
        <ion-label slot="start">
          <p>
            <label class="bold-text">{this.institution.name}</label>
            <br/>
            {this.institution.users_count} Member(s)
          </p>
        </ion-label>
        <ion-label slot="start">
          <p>
            {this.institution.address}
          </p>
        </ion-label>
        <ion-button slot="end" size="default" fill="clear"
                    onClick={(evt) => this.onOptionsButtonClicked(evt)}>
          <ion-icon slot="icon-only" name="more"/>
        </ion-button>
      </ion-item>
    ];
  }
}
