import {Component, h} from '@stencil/core';
import _ from 'underscore';

@Component({
  tag: 'app-dashboard-page',
  styleUrl: 'app-dashboard-page.css'
})
export class AppDashboardPage {

  cards = [
    {
      title: "Today's Events",
      getComponent: () => <app-dashboard-today/>
    }
  ];

  renderCardGrid() {
    let rowIndex = 0;
    let colIndex = 0;
    const rows = _.groupBy(this.cards, () => {
      if (++colIndex < 3) {
        return rowIndex;
      } else {
        colIndex = 0;
        return ++rowIndex;
      }
    });
    return (
      <ion-grid>
        {Object.values(rows).map((cards: any[]) => {
          return (<ion-row>
            {cards.map((card) => {
              return (
                <ion-col>
                  <ion-card style={{background: 'white'}}>
                    <ion-card-header>
                      <ion-card-title>
                        {card.title}
                      </ion-card-title>
                    </ion-card-header>
                    <ion-card-content>
                      {card.getComponent()}
                    </ion-card-content>
                  </ion-card>
                </ion-col>
              )
            })}
          </ion-row>)
        })}
      </ion-grid>
    )
  }

  render() {
    return [
      <ion-content class="ion-padding background-page scroll-padding">
        {this.renderCardGrid()}
      </ion-content>
    ];
  }
}
