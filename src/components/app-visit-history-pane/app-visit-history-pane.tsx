import {Component, h} from '@stencil/core';

@Component({
  tag: 'app-visit-history-pane',
  styleUrl: 'app-visit-history-pane.css'
})
export class AppVisitHistoryPane {

  render() {
    return [
      <ion-card style={{'--background': 'white'}}>
        <ion-list>
          <ion-item lines="none">
            <ion-label>
              <p>
                <b style={{'font-size': '1.2em'}}>
                  Today
                </b>
                <br/>
                9:00am - 10:00am
              </p>
            </ion-label>
            <ion-label class="ion-text-center">
              Morning Campus Tour
            </ion-label>
            <ion-label class="ion-text-right">
              <p>
                Admissions Office
              </p>
            </ion-label>
          </ion-item>
        </ion-list>
      </ion-card>
    ];
  }
}
