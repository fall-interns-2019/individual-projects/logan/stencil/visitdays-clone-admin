import {Component, Element, h} from '@stencil/core';
import $ from 'jquery';
import {AppRoot} from "../app-root/app-root";

@Component({
  tag: 'app-login-page',
  styleUrl: 'app-login-page.css'
})
export class AppLoginPage {

  @Element() el: HTMLElement;

  private formInputs = {};
  private form: HTMLFormElement;
  private debounce = false;

  private async onLoginClick() {
    if (this.debounce || !this.form.reportValidity()) {
      return;
    }

    this.debounce = true;

    let button = $('#button-login')[0] as HTMLIonButtonElement;
    button.disabled = true;

    let email = this.formInputs['input-email'];
    let password = this.formInputs['input-password'];

    let result = await AppRoot.getApiHelper().emailSignIn({email, password})
      .catch((err) => {
        AppRoot.showNotification(err, 'danger');
      });

    this.debounce = false;
    button.disabled = false;

    if (!result) return;

    console.log('result:', result);
    await AppRoot.route('dashboard');
  }

  private async onKeyDown(event) {
    if (event.key === 'Enter')
      await this.onLoginClick();
  }

  private handleFormInputChanged(event) {
    this.formInputs[event.target.id] = event.detail.value;
  }

  render() {
    return [
      <ion-content color="light" fullscreen>
        <ion-grid>
          <ion-row style={{height: '100vh'}} align-items-center justify-content-around>
            <ion-col>
              <ion-title>
                VisitDays Clone - Admin Panel
              </ion-title>
              <form ref={(el) => this.form = el}>
                <ion-list style={{'background': 'var(--ion-color-light)'}} class="ion-padding">
                  <ion-item color="light" lines="inset">
                    <ion-input id="input-email" required type="email" placeholder="Email Address"
                               onKeyDown={(evt) => this.onKeyDown(evt)}
                               onIonChange={(evt) => this.handleFormInputChanged(evt)}/>
                  </ion-item>
                  <ion-item color="light" lines="inset">
                    <ion-input id="input-password" required type="password" placeholder="Password"
                               onKeyDown={(evt) => this.onKeyDown(evt)}
                               onIonChange={(evt) => this.handleFormInputChanged(evt)}/>
                  </ion-item>
                </ion-list>
                <ion-button id="button-login" class="ion-padding center-horiz button-margin half-width"
                            color="primary" onClick={() => this.onLoginClick()}>
                  Login
                </ion-button>
                <ion-label class="ion-text-center center-horiz">Don't have an account?</ion-label>
                <ion-button href="#/register" class="center-horiz" fill="clear">
                  <ion-label color="primary">Register</ion-label>
                </ion-button>
              </form>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    ];
  }
}
