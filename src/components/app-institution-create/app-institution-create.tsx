import {Component, Element, h, Prop} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";
import $ from 'jquery';
import {modalController} from "@ionic/core";

@Component({
  tag: 'app-institution-create',
  styleUrl: 'app-institution-create.css'
})
export class AppInstitutionCreate {

  @Element() el: HTMLElement;

  @Prop() institution: any;

  private formInputs = {};
  private form: HTMLFormElement;
  private debounce = false;

  async onCancelClicked() {
    await modalController.dismiss();
  }

  async onAddInstitutionClicked() {
    if (this.debounce || !this.form.reportValidity()) {
      return;
    }

    this.debounce = true;

    const name = this.formInputs['input-institution-name'];
    const address = this.formInputs['input-institution-address'];

    const button = $('#button-add-institution')[0] as HTMLIonButtonElement;
    button.disabled = true;

    const result = await AppRoot.getInstitutionHelper().upsert(
      this.institution ? this.institution.id : null,
      {
        name, address
      })
      .catch((err) => {
        AppRoot.showNotification(err, 'danger');
      });

    this.debounce = false;
    button.disabled = false;

    if (!result) return;

    return modalController.dismiss(result.data)
  }

  private handleFormInputChanged(event) {
    this.formInputs[event.target.id] = event.detail.value;
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-title>{this.institution ? 'Edit' : 'Create'} Institution</ion-title>
          <ion-buttons slot="primary">
            <ion-button color="primary" onClick={() => this.onCancelClicked()}>Cancel</ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-content color="light">
        <ion-card style={{'--background': '#ffffff'}}>
          <ion-card-content class="ion-no-padding">
            <form ref={(el) => this.form = el}>
              <ion-list lines="none">
                <ion-item color="light" lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Institution name</ion-label>
                  <ion-input id="input-institution-name" required type="text" color="medium"
                             placeholder={
                               !this.institution ? "e.g., Fullmeasure University" : null
                             }
                             value={this.institution ? this.institution.name : null}
                             onIonChange={(evt) => this.handleFormInputChanged(evt)}/>
                </ion-item>
                <ion-item color="light" lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Institution address</ion-label>
                  <ion-input id="input-institution-address" required type="text" color="medium"
                             placeholder={
                               !this.institution ? "e.g., 1234 1st Street, Washington, DC 20002" : null
                             }
                             value={this.institution ? this.institution.address : null}
                             onIonChange={(evt) => this.handleFormInputChanged(evt)}/>
                </ion-item>
              </ion-list>
            </form>
            <ion-button id="button-add-institution" class="ion-padding center-horiz half-width"
                        color="primary"
                        onClick={() => this.onAddInstitutionClicked()}>
              {this.institution ? 'Edit' : 'Create'} Institution
            </ion-button>
          </ion-card-content>
        </ion-card>
      </ion-content>
    ];
  }
}
