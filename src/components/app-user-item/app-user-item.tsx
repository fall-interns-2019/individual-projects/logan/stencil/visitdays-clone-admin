import {Component, Event, EventEmitter, h, Prop} from '@stencil/core';
import {popoverController} from "@ionic/core";
import {AppRoot} from "../app-root/app-root";

@Component({
  tag: 'app-user-item',
  styleUrl: 'app-user-item.css'
})
export class AppUserItem {

  @Prop() user: any;

  @Event() userEvent: EventEmitter;

  async onOptionsButtonClicked(event) {
    const popover = await popoverController.create({
      component: 'app-popover',
      componentProps: {
        title: 'Actions',
        buttons: [
          {text: 'Delete', color: 'danger', action: 'delete', role: 'admin'}
        ]
      },
      showBackdrop: false,
      event
    });

    await popover.present();
    const {data} = await popover.onWillDismiss();
    if (!data) return;

    this.userEvent.emit({data, user: this.user})
  }

  render() {
    return [
      <ion-item lines="none">
        <div slot="start" class="color-circle-outer">
          <div class="color-circle-inner" style={{background: 'var(--ion-color-primary)'}}/>
        </div>
        <ion-label slot="start">
          <p>
            <label class="bold-text">{this.user.first_name} {this.user.last_name}</label>
          </p>
        </ion-label>
        <ion-label slot="start">
          {this.user.institution ? (
            <p>
              <label class="bold-text">{this.user.institution.name}:</label>
            </p>
          ) : null}
          <p>
            Role: {AppRoot.getPermissionsHelper().getHighestRole(this.user).name}
          </p>
        </ion-label>
        <ion-button slot="end" size="default" fill="clear"
                    onClick={(evt) => this.onOptionsButtonClicked(evt)}>
          <ion-icon slot="icon-only" name="more"/>
        </ion-button>
      </ion-item>
    ];
  }
}
