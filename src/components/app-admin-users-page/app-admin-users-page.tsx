import {Component, Element, h, Listen, State} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";
import {alertController} from "@ionic/core";

@Component({
  tag: 'app-admin-users-page',
  styleUrl: 'app-admin-users-page.css'
})
export class AppAdminUsersPage {

  @Element() el: HTMLElement;

  @State() users = [];

  @Listen('userEvent')
  async handleUserEvent(event: CustomEvent) {
    const {data, user} = event.detail;

    if(data.action === 'edit') {
      // await this.onNewInstitutionClicked(institution)
    } else if(data.action === 'delete') {
      await this.onDeleteUserClicked(user)
    }
  }

  async componentWillLoad() {
    const {data} = await AppRoot.getUserHelper().requestAll();
    if(!data) return;

    console.log(data);

    this.users = data.sort((a, b) => new Date(a.created_by) < new Date(b.created_by) ? 1 : -1);
  }

  async onDeleteUserClicked(user) {
    const alert = await alertController.create({
      header: 'Confirmation',
      message: 'Are you sure you want to <strong>delete</strong> this user?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Delete',
          role: 'delete'
        }
      ]
    });

    await alert.present();
    const result = await alert.onWillDismiss();

    if (result.role !== 'delete') return;

    const delete_result = await AppRoot.getUserHelper().destroy(user.id)
      .catch((err) => {
        AppRoot.showNotification(err, 'danger');
      });

    if (!delete_result) return;

    await this.componentWillLoad();
  }

  private renderUsers() {
    return this.users.map((user) => {
      return <app-user-item user={user} />
    })
  }

  render() {
    return [
      <ion-content id="menu-content" class="ion-padding background-page">
        <ion-card>
          <ion-item lines="none">
            <ion-button fill="clear" color="primary" class="button-hover-selected">
              Users
            </ion-button>
            <ion-searchbar slot="end" style={{'width': '50%'}}/>
          </ion-item>
          <ion-list lines="none">
            {this.renderUsers()}
          </ion-list>
        </ion-card>
      </ion-content>
    ];
  }
}
