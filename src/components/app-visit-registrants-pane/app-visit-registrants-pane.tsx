import {Component, h} from '@stencil/core';

@Component({
  tag: 'app-visit-registrants-pane',
  styleUrl: 'app-visit-registrants-pane.css'
})
export class AppVisitRegistrantsPane {

  render() {
    return [
      <ion-card>
        <ion-item lines="none">
          <ion-label slot="start">Campus Tour</ion-label>
          <ion-label slot="end" text-wrap class="ion-text-right">
            <p>
              September 19
              <br/>
              11:00 am - 12:00 pm
            </p>
          </ion-label>
        </ion-item>
        <ion-item lines="none">
          <ion-avatar slot="start">
            <img src="/assets/male-face-icon.png"/>
          </ion-avatar>
          <ion-avatar slot="start">
            <img src="/assets/female-face-icon.png"/>
          </ion-avatar>
          <ion-label slot="end">
            <b class="ion-form-text">14</b>
            <br/>
            <p>Prosp. Students</p>
          </ion-label>
          <ion-label slot="end">
            <b class="ion-form-text">18</b>
            <br/>
            <p>Guests</p>
          </ion-label>
          <ion-label slot="end">
            <b class="ion-form-text">32</b>
            <br/>
            <p>Total Registrants</p>
          </ion-label>
          <ion-label slot="end">
            <b class="ion-form-text">64%</b>
            <br/>
            <p>Capacity Filled</p>
          </ion-label>
        </ion-item>
        <ion-item lines="none" style={{'--background': 'rgba(var(--ion-color-primary-rgb), 0.1)'}}>
          <ion-label slot="start">
            <p>3 (21%) Checked In</p>
          </ion-label>
        </ion-item>
        <ion-progress-bar value={0.21}/>
      </ion-card>,
      <ion-card>
        <ion-item lines="none">
          <ion-button fill="clear" color="primary" class="button-hover-selected">
            Registered (2)
          </ion-button>
          <ion-button fill="clear" color="medium" class="button-hover">
            Canceled (1)
          </ion-button>
          <ion-searchbar slot="end" style={{'width': '50%'}}/>
        </ion-item>
        <ion-item lines="none">
          <ion-button fill="clear" style={{'--color': 'var(--ion-color-primary-tint)'}}
                      class="ion-padding-start button-hover-selected">
            All
          </ion-button>
          <ion-button fill="clear" style={{'--color': 'var(--ion-color-medium-tint)'}}
                      class="button-hover">
            Checked In
          </ion-button>
          <ion-button fill="clear" style={{'--color': 'var(--ion-color-medium-tint)'}}
                      class="button-hover">
            Not Here
          </ion-button>
        </ion-item>
        <ion-list lines="none">
          <app-registrant-item/>
          <app-registrant-item/>
        </ion-list>
      </ion-card>
    ];
  }
}
