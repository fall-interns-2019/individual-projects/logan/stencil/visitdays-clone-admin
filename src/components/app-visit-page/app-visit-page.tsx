import {Component, h, State} from '@stencil/core';

@Component({
  tag: 'app-visit-page',
  styleUrl: 'app-visit-page.css'
})
export class AppVisitPage {

  @State() selected = 'registrants';

  panes = {
    registrants: {title: 'Registrants', getComponent: () => <app-visit-registrants-pane/>},
    details: {title: 'Details', getComponent: () => <app-visit-details-pane/>},
    history: {title: 'History', getComponent: () => <app-visit-history-pane/>}
  };

  renderPaneButtons() {
    return Object.keys(this.panes).map((key) => {
      const data = this.panes[key];
      return (
        <ion-button fill="clear" color={this.selected === key ? 'primary' : 'medium'}
                    class={`button-hover${this.selected === key ? '-selected' : ''}`}
                    onClick={() => this.selected = key}>
          {data.title}
        </ion-button>
      )
    });
  }

  renderPane() {
    return this.panes[this.selected].getComponent();
  }

  render() {
    return [
      <ion-content class="ion-padding background-page scroll-padding">
        <ion-item class="ion-no-padding background-page">
          {this.renderPaneButtons()}
        </ion-item>
        {this.renderPane()}
      </ion-content>
    ];
  }
}
