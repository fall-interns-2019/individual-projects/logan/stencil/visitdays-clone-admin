import {Component, Element, h} from '@stencil/core';
import $ from 'jquery';
import {AppRoot} from "../app-root/app-root";

@Component({
  tag: 'app-register-page',
  styleUrl: 'app-register-page.css'
})
export class AppRegisterPage {

  @Element() el: HTMLElement;

  private institutions = [];
  private formInputs = {};
  private form: HTMLFormElement;
  private debounce = false;

  async componentWillLoad() {
    const {data} = await AppRoot.getInstitutionHelper().requestAll();
    if(!data) return;

    this.institutions = data.sort((a, b) => new Date(a.name) < new Date(b.name) ? 1 : -1);
  }

  private async onCreateAccountClick() {
    if(this.debounce || !this.form.reportValidity()) {
      return;
    }

    this.debounce = true;

    let button = $('#button-create')[0] as HTMLIonButtonElement;
    button.disabled = true;

    let institution_id = this.formInputs['input-institution-id'];
    let first_name = this.formInputs['input-first-name'];
    let last_name = this.formInputs['input-last-name'];
    let email = this.formInputs['input-email'];
    let password = this.formInputs['input-password'];

    let result = await AppRoot.getApiHelper().emailSignUp(
      {email, password, password_confirmation: password},
      {first_name, last_name, institution_id}
    )
      .catch((err) => {
        AppRoot.showNotification(err, 'danger');
      });

    this.debounce = false;
    button.disabled = false;

    if(!result) return;

    console.log('result:', result);
    await AppRoot.route('dashboard');
  }

  private async onKeyDown(event) {
    if(event.key === 'Enter')
      await this.onCreateAccountClick();
  }

  private handleFormInputChanged(event) {
    this.formInputs[event.target.id] = event.detail.value;
  }

  private renderInstitutions() {
    return this.institutions.map((inst) => {
      return (
        <ion-select-option value={inst.id}>{inst.name}</ion-select-option>
      )
    })
  }

  render() {
    return [
      <ion-content color="light" fullscreen>
        <ion-grid>
          <ion-row style={{height: '100vh'}} align-items-center justify-content-around>
            <ion-col>
              <form ref={(el) => this.form = el}>
                <ion-list style={{'background': 'var(--ion-color-light)'}} class="ion-padding">
                  <ion-item color="light" lines="none">
                    <ion-label color="medium" position="fixed">Institution</ion-label>
                    <ion-select id="input-institution-id" color="primary"
                                onIonChange={(evt) => this.handleFormInputChanged(evt)}>
                      {this.renderInstitutions()}
                    </ion-select>
                  </ion-item>
                  <ion-item color="light" lines="inset">
                    <ion-label color="primary" position="stacked">First Name</ion-label>
                    <ion-input id="input-first-name" required type="text"
                               onIonChange={(evt) => this.handleFormInputChanged(evt)}/>
                  </ion-item>
                  <ion-item color="light" lines="inset">
                    <ion-label color="primary" position="stacked">Last Name</ion-label>
                    <ion-input id="input-last-name" required type="text"
                               onIonChange={(evt) => this.handleFormInputChanged(evt)}/>
                  </ion-item>
                  <ion-item color="light" lines="inset">
                    <ion-input id="input-email" required type="email" placeholder="Email Address"
                               onIonChange={(evt) => this.handleFormInputChanged(evt)}/>
                  </ion-item>
                  <ion-item color="light" lines="inset">
                    <ion-input id="input-password" required type="password" placeholder="Password"
                               onKeyDown={(evt) => this.onKeyDown(evt)}
                               onIonChange={(evt) => this.handleFormInputChanged(evt)}/>
                  </ion-item>
                </ion-list>
                <ion-button id="button-create" class="ion-padding center-horiz button-margin half-width"
                            color="primary" onClick={() => this.onCreateAccountClick()}>
                  Create Account
                </ion-button>
                <ion-label class="ion-text-center center-horiz">Already have an account?</ion-label>
                <ion-button href="#/login" style={{'display': 'table'}} class="center-horiz" fill="clear">
                  <ion-label color="primary">Sign In</ion-label>
                </ion-button>
              </form>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    ]
  }
}
