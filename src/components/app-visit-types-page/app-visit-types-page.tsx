import {Component, Element, h, Listen, State} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";
import {alertController, modalController} from "@ionic/core";
import {SessionService} from "../../services/session.service";

@Component({
  tag: 'app-visit-types-page',
  styleUrl: 'app-visit-types-page.css'
})
export class AppVisitTypesPage {

  @Element() el: HTMLElement;

  @State() visit_types = [];

  @Listen('visitTypeEvent')
  async handleEvent(event: CustomEvent) {
    const {data, visit_type} = event.detail;

    if(data.action === 'edit') {
      await this.onNewVisitTypeClicked(visit_type)
    } else if(data.action === 'delete') {
      await this.onDeleteVisitTypeClicked(visit_type)
    }
  }

  async componentWillLoad() {
    const {data} = await AppRoot.getVisitTypeHelper().requestAll(
      `institution_id=${SessionService.get().institution_id}`
    );
    if(!data) return;

    this.visit_types = data.sort((a, b) => new Date(a.created_by) < new Date(b.created_by) ? 1 : -1);
  }

  async onNewVisitTypeClicked(visit_type?) {
    const modal = await modalController.create({
      component: 'app-visit-type-create',
      componentProps: {
        modalController: modalController,
        visit_type
      }
    });

    await modal.present();

    const {data} = await modal.onWillDismiss();
    if(!data) return;

    const copy = [...this.visit_types];

    if(visit_type) {
      const index = copy.findIndex((obj) => obj.id === visit_type.id);
      copy[index] = data;
    } else {
      copy.push(data);
    }

    this.visit_types = copy
      .sort((a, b) => new Date(a.created_by) < new Date(b.created_by) ? 1 : -1);
  }

  async onDeleteVisitTypeClicked(visit_type) {
    const alert = await alertController.create({
      header: 'Confirmation',
      message: 'Are you sure you want to <strong>delete</strong> this visit type?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Delete',
          role: 'delete'
        }
      ]
    });

    await alert.present();
    const result = await alert.onWillDismiss();

    if (result.role !== 'delete') return;

    const delete_result = await AppRoot.getVisitTypeHelper().destroy(visit_type.id)
      .catch((err) => {
        AppRoot.showNotification(err, 'danger');
      });

    if (!delete_result) return;

    await this.componentWillLoad();
  }

  private renderVisitTypes() {
    return this.visit_types.map((visit_type) => {
      return <app-visit-type-item visit_type={visit_type} />
    })
  }

  render() {
    return [
      <ion-content id="menu-content" class="ion-padding background-page">
        <ion-card>
          <ion-item lines="none">
            <ion-button fill="clear" color="primary" class="button-hover-selected">
              Visit Types
            </ion-button>
            <ion-searchbar slot="end" style={{'width': '50%'}}/>
            <ion-button slot="end" color="primary" size="default"
                        onClick={() => this.onNewVisitTypeClicked()}>
              Create
            </ion-button>
          </ion-item>
          <ion-list lines="none">
            {this.renderVisitTypes()}
          </ion-list>
        </ion-card>
      </ion-content>
    ];
  }
}
