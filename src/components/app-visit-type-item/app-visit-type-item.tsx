import {Component, Event, EventEmitter, h, Prop} from '@stencil/core';
import {popoverController} from "@ionic/core";

@Component({
  tag: 'app-visit-type-item',
  styleUrl: 'app-visit-type-item.css'
})
export class AppVisitTypeItem {

  @Prop() visit_type: any;

  @Event() visitTypeEvent: EventEmitter;

  async onOptionsButtonClicked(event) {
    const popover = await popoverController.create({
      component: 'app-popover',
      componentProps: {
        title: 'Actions',
        buttons: [
          {text: 'Edit', color: 'warning', action: 'edit', role: 'manager'},
          {text: 'Delete', color: 'danger', action: 'delete', role: 'manager'}
        ]
      },
      showBackdrop: false,
      event
    });

    await popover.present();
    const {data} = await popover.onWillDismiss();
    if (!data) return;

    this.visitTypeEvent.emit({data, visit_type: this.visit_type})
  }

  render() {
    return [
      <ion-item lines="none">
        <div slot="start" class="color-circle-outer">
          <div class="color-circle-inner" style={{background: 'var(--ion-color-primary)'}}/>
        </div>
        <ion-label slot="start">
          <p>
            <label class="bold-text">{this.visit_type.name}</label>
          </p>
        </ion-label>
        <ion-button slot="end" size="default" fill="clear"
                    onClick={(evt) => this.onOptionsButtonClicked(evt)}>
          <ion-icon slot="icon-only" name="more"/>
        </ion-button>
      </ion-item>
    ];
  }
}
