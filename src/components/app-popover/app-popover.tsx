import {Component, h, Prop} from '@stencil/core';
import {popoverController} from "@ionic/core";
import {AppRoot} from "../app-root/app-root";

@Component({
  tag: 'app-popover',
  styleUrl: 'app-popover.css'
})
export class AppPopover {

  @Prop() title: string;
  @Prop() buttons: [{ text: string, color: string, action: string, role: string }];

  renderButtons() {
    return this.buttons.map((buttonInfo) => {
      if(buttonInfo.role && !AppRoot.getPermissionsHelper().hasRole(buttonInfo.role)) return;
      return (
        <ion-item button onClick={() => popoverController.dismiss({action: buttonInfo.action})}>
          <ion-label color={buttonInfo.color}>{buttonInfo.text}</ion-label>
        </ion-item>
      )
    });
  }

  render() {
    return [
      <ion-list lines="none">
        {this.title ? (
          <ion-item-divider>
            {this.title}
          </ion-item-divider>
        ) : null}
        {this.renderButtons()}
      </ion-list>
    ];
  }
}
