import {Component, Element, h, Method, Prop, State} from '@stencil/core';
import {toastController} from "@ionic/core";
import {ApiHelper} from "../../helpers/apiHelper";
import {InstitutionHelper} from "../../helpers/institutionHelper";
import {PermissionsHelper} from "../../helpers/permissionsHelper";
import {UserHelper} from "../../helpers/userHelper";
import {VisitTypeHelper} from "../../helpers/visitTypeHelper";
import {RouteService} from "../../services/route.service";

@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css'
})
export class AppRoot {

  @Element() el: HTMLElement;

  @Prop() ApiHelper;
  @Prop() PermissionsHelper;
  @Prop() InstitutionHelper;
  @Prop() VisitTypeHelper;
  @Prop() UserHelper;
  @State() route: any;

  static routes = [
    {
      url: '',
      access: '',
      redirect: 'dashboard'
    },
    {
      url: 'dashboard',
      menu: {
        name: 'Dashboard',
        icon: 'clipboard',
        header: 'Visits'
      },
      access: 'default',
      getComponent: () => <app-dashboard-page/>
    },
    {
      url: 'calendar',
      menu: {
        name: 'Calendar',
        icon: 'calendar',
        header: 'Visits'
      },
      access: 'member',
      getComponent: () => <app-calendar-page/>
    },
    // management pages
    {
      url: 'visit-types',
      menu: {
        name: 'Visit Types',
        icon: 'menu',
        header: 'Management'
      },
      access: 'manager',
      getComponent: () => <app-visit-types-page/>
    },
    // admin pages
    {
      url: 'institutions',
      menu: {
        name: 'Institutions',
        icon: 'business',
        header: 'Admin'
      },
      access: 'admin',
      getComponent: () => <app-admin-institutions-page/>
    },
    {
      url: 'users',
      menu: {
        name: 'Users',
        icon: 'people',
        header: 'Admin'
      },
      access: 'admin',
      getComponent: () => <app-admin-users-page/>
    },
    {
      url: 'register',
      access: '',
      getComponent: () => <app-register-page/>
    },
    {
      url: 'login',
      access: '',
      getComponent: () => <app-login-page/>
    }
  ];

  constructor() {
    this.ApiHelper = new ApiHelper();
    this.PermissionsHelper = new PermissionsHelper();
    this.InstitutionHelper = new InstitutionHelper();
    this.VisitTypeHelper = new VisitTypeHelper();
    this.UserHelper = new UserHelper();
  }

  async componentWillLoad() {
    await this.onRouteChanged();
    window.addEventListener('hashchange', async () => {
      console.log('route will change');
      await this.onRouteChanged();
      console.log('route changed');
    });
  }

  @Method() async updateRoute(route) {
    this.route = route;
  }

  static async showNotification(message, color?, waitForDismiss?) {
    const toast = await toastController.create({
      message, color,
      duration: 2000,
      position: 'middle',
    });

    await toast.present();

    if (waitForDismiss)
      await toast.onWillDismiss();
  }

  static getAppRoot() {
    return document.querySelector('app-root');
  }

  static getApiHelper() {
    return document.querySelector('app-root').ApiHelper;
  }

  static getPermissionsHelper() {
    return document.querySelector('app-root').PermissionsHelper;
  }

  static getInstitutionHelper() {
    return document.querySelector('app-root').InstitutionHelper;
  }

  static getVisitTypeHelper() {
    return document.querySelector('app-root').VisitTypeHelper;
  }

  static getUserHelper() {
    return document.querySelector('app-root').UserHelper;
  }

  static getUser() {
    return AppRoot.getApiHelper().getUser();
  }

  static getRouteInfo(url = RouteService.path()) {
    if(url.includes('?')) {
      url = url.split('?')[0];
    }
    return AppRoot.routes.find((routeInfo) => {
      return routeInfo.url === url;
    }) as any
  }

  static getRoutes() {
    return this.routes;
  }

  static async appendInstitution(url: string) {
    const institution = await AppRoot.getCurrentInstitution();
    return institution ? url
        .concat(url.includes('?') ? '&' : '?')
        .concat(`institution_id=${institution.id}`)
      : url;
  }

  static async route(url: string, keepHistory?: boolean) {
    url = await AppRoot.appendInstitution(url);
    const route = AppRoot.getRouteInfo(url);
    history[keepHistory ? 'pushState' : 'replaceState'](null, null, `#/${url}`);
    console.log('hello 1');
    await AppRoot.getAppRoot().updateRoute(route);
    console.log('hello 2');
    // this.route = route;
  }

  async doRoute(url: string, keepHistory?: boolean) {
    url = await AppRoot.appendInstitution(url);
    const route = AppRoot.getRouteInfo(url);
    history[keepHistory ? 'pushState' : 'replaceState'](null, null, `#/${url}`);
    this.route = route;
    console.log('this.route:', this.route);
  }

  static async checkAccess(routePath) {
    let routeInfo = AppRoot.getRouteInfo(routePath);
    if (!routeInfo) return false;

    const validUser = await AppRoot.getApiHelper().validateUser()
      .catch(_ => {
      });
    const hasAccess = AppRoot.getPermissionsHelper().hasRole(routeInfo.access);
    let destination;

    if (routeInfo.access === '' && validUser) {
      destination = '';
    } else {
      if (!hasAccess) {
        destination = 'login';
      }
    }

    return [!destination, destination] as any;
  }

  static async verifyAccess(routePath = RouteService.path()) {
    const [access, redirect] = await AppRoot.checkAccess(routePath);

    console.log('routing: trying (', routePath, ') access(', access, ') redirect(', redirect, ')');

    if (!access) {
      console.log('routing: no access! will redirect!');
      window.location.href = `#/${redirect}`;
      return false;
    }

    return true;
  }

  async onRouteChanged() {
    console.log('onRouteChanged');
    const route = AppRoot.getRouteInfo();
    if (!route) return;
    if (route.redirect) {
      console.log('redirecting to', route.redirect);
      window.location.href = `#/${route.redirect}`;
      return;
    }

    const allowed = await AppRoot.verifyAccess();
    if (!allowed) return;

    console.log('going to', route.url);
    await this.doRoute(route.url, true);
    console.log('route set to', this.route);
  }

  static async getCurrentInstitution() {
    return await AppRoot.getInstitutionHelper().getCurrent();
  }

  renderComponent() {
    console.log('trying to render...');
    if (!this.route) {
      console.log('no route found');
      return;
    }
    console.log('loading', this.route.url);
    return this.route.menu ? <app-menu/> : this.route.getComponent();
  }

  render() {
    return (
      <ion-app>
        {this.renderComponent()}
      </ion-app>
    );
  }
}
