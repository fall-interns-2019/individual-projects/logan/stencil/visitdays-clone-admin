import {Component, h} from '@stencil/core';
import {addDays, format} from 'date-fns';

@Component({
  tag: 'app-calendar',
  styleUrl: 'app-calendar.css'
})
export class AppCalendar {

  day_names = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

  renderRows() {
    let first_day = new Date();
    first_day.setDate(1);
    while (!format(first_day, 'eee').match('Su')) {
      first_day = addDays(first_day, -1);
    }
    const today = new Date();
    let index = 0;
    const rows = new Array(5).fill(null);
    return rows.map((_, rowIndex) => {
      const cols = new Array(7).fill(null);
      return (
        <ion-row>
          {cols.map((_, colIndex) => {
            const isToday = addDays(first_day, index).toDateString() === today.toDateString();
            const col = (
              <ion-col>
                <ion-item>
                  <ion-label color={isToday ? 'primary' : ''} class="ion-text-center">
                    <p>
                      {rowIndex === 0 ? this.day_names[colIndex] : null}
                      <br/>
                      {format(addDays(first_day, index), 'd')}
                    </p>
                  </ion-label>
                </ion-item>
              </ion-col>
            );
            index++;
            return col;
          })}
        </ion-row>
      )
    });
  }

  render() {
    return [
      <ion-grid>
        {this.renderRows()}
      </ion-grid>
    ];
  }
}
