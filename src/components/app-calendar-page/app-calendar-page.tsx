import {Component, h} from '@stencil/core';

@Component({
  tag: 'app-calendar-page',
  styleUrl: 'app-calendar-page.css'
})
export class AppCalendarPage {

  render() {
    return [
      <ion-content class="ion-padding background-page scroll-padding">
        <app-calendar/>
      </ion-content>
    ];
  }
}
