import {Component, h, State} from '@stencil/core';
import {getCSSValue} from "../../helpers/utils";
import {AppRoot} from "../app-root/app-root";
import {popoverController} from "@ionic/core";

@Component({
  tag: 'app-header',
  styleUrl: 'app-header.css'
})
export class AppHeader {

  @State() institution: any;
  private institutions: any[] = [];

  async componentWillLoad() {
    await AppRoot.getApiHelper().waitForUser();
    const {data} = await AppRoot.getInstitutionHelper().requestAll();
    if(!data) return;

    this.institutions = data.sort((a, b) => new Date(a.created_by) < new Date(b.created_by) ? 1 : -1);
    this.institution = await this.getCurrentInstitution();
  }

  async getCurrentInstitution() {
    return await AppRoot.getCurrentInstitution();
  }

  async onAvatarButtonClicked(event) {
    const popover = await popoverController.create({
      component: 'app-popover',
      componentProps: {
        title: `${AppRoot.getUser().data.first_name} ${AppRoot.getUser().data.last_name}`,
        buttons: [
          {text: 'Logout', color: 'danger', action: 'logout'}
        ]
      },
      event
    });

    await popover.present();
    const {data} = await popover.onWillDismiss();
    if(!data) return;

    if(data.action === 'logout') {
      const result = await AppRoot.getApiHelper().signOut()
        .catch((err) => {
          AppRoot.showNotification(err, 'danger');
        });

      if (!result) return;

      await AppRoot.route('login');
    }
  }

  renderInstitutionOptions() {
    return [
      <ion-select-option value={-1}>No institution</ion-select-option>
    ].concat(this.institutions.map((institution) => {
      return (
        <ion-select-option value={institution.id}>
          {institution.name}
        </ion-select-option>
      )
    }));
  }

  async onInstitutionChanged(event) {
    AppRoot.getInstitutionHelper().setCurrent(this.institutions.find(
      (institution) => institution.id === event.detail.value));

    console.log('changed institution to', await this.getCurrentInstitution());
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar color="light">
          <ion-select interface="popover" onIonChange={(evt) => this.onInstitutionChanged(evt)}
                      value={this.institution ? this.institution.id : -1}
                      placeholder={!this.institution ? 'No institution selected' : null}>
            {this.renderInstitutionOptions()}
          </ion-select>
          <ion-buttons slot="primary">
            <ion-button fill="clear" shape="round" class="center-horiz"
                        onClick={(evt) => this.onAvatarButtonClicked(evt)}>
              <ion-avatar slot="start">
                <img src={`https://ui-avatars.com/api/
                ?name=${AppRoot.getUser().data.first_name}
                +${AppRoot.getUser().data.last_name}
                &rounded=true&background=${getCSSValue('--ion-color-primary')}&color=ffffff`}/>
              </ion-avatar>
              <ion-icon slot="end" name="arrow-dropdown"/>
            </ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>
    ];
  }
}
