import {Component, Element, h, Listen, State} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";
import {alertController, modalController} from "@ionic/core";

@Component({
  tag: 'app-admin-institutions-page',
  styleUrl: 'app-admin-institutions-page.css'
})
export class AppAdminInstitutionsPage {

  @Element() el: HTMLElement;

  @State() institutions = [];

  @Listen('institutionEvent')
  async handleInstitutionEvent(event: CustomEvent) {
    const {data, institution} = event.detail;

    if(data.action === 'edit') {
      await this.onNewInstitutionClicked(institution)
    } else if(data.action === 'delete') {
      await this.onDeleteInstitutionClicked(institution)
    }
  }

  async componentWillLoad() {
    const {data} = await AppRoot.getInstitutionHelper().requestAll();
    if(!data) return;

    this.institutions = data.sort((a, b) => new Date(a.created_by) < new Date(b.created_by) ? 1 : -1);
  }

  async onNewInstitutionClicked(institution?) {
    const modal = await modalController.create({
      component: 'app-institution-create',
      componentProps: {
        modalController: modalController,
        institution
      }
    });

    await modal.present();

    const {data} = await modal.onWillDismiss();
    if(!data) return;

    const copy = [...this.institutions];

    if(institution) {
      const index = copy.findIndex((inst) => inst.id === institution.id);
      copy[index] = data;
    } else {
      copy.push(data);
    }

    this.institutions = copy
      .sort((a, b) => new Date(a.created_by) < new Date(b.created_by) ? 1 : -1);
  }

  async onDeleteInstitutionClicked(institution) {
    const alert = await alertController.create({
      header: 'Confirmation',
      message: 'Are you sure you want to <strong>delete</strong> this institution?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Delete',
          role: 'delete'
        }
      ]
    });

    await alert.present();
    const result = await alert.onWillDismiss();

    if (result.role !== 'delete') return;

    const delete_result = await AppRoot.getInstitutionHelper().destroy(institution.id)
      .catch((err) => {
        AppRoot.showNotification(err, 'danger');
      });

    if (!delete_result) return;

    await this.componentWillLoad();
  }

  private renderInstitutions() {
    return this.institutions.map((institution) => {
      return <app-institution-item institution={institution} />
    })
  }

  render() {
    return [
      <ion-content id="menu-content" class="ion-padding background-page">
        <ion-card>
          <ion-item lines="none">
            <ion-button fill="clear" color="primary" class="button-hover-selected">
              Institutions
            </ion-button>
            <ion-searchbar slot="end" style={{'width': '50%'}}/>
            <ion-button slot="end" color="primary" size="default"
                        onClick={() => this.onNewInstitutionClicked()}>
              Create
            </ion-button>
          </ion-item>
          <ion-list lines="none">
            {this.renderInstitutions()}
          </ion-list>
        </ion-card>
      </ion-content>
    ];
  }
}
