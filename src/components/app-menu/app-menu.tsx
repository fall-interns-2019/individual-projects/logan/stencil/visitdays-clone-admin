import {Component, Element, h, State} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";
import {RouteService} from "../../services/route.service";

@Component({
  tag: 'app-menu',
  styleUrl: 'app-menu.css'
})
export class AppMenu {

  @Element() el: HTMLElement;
  @State() component: any;

  async componentWillLoad() {
    await AppRoot.getApiHelper().waitForUser();
  }

  async componentWillRender() {
    const routeInfo = AppRoot.getRouteInfo(RouteService.path());
    const access = await AppRoot.verifyAccess(RouteService.path());
    if(!access) return;

    this.component = routeInfo.getComponent();

    window.onpopstate = () => {
      this.component = routeInfo.getComponent();
    }
  }

  async onMenuItemClicked(route) {
    const institution = await AppRoot.getCurrentInstitution();
    history.pushState(route, null, `#/${route}${
      institution ? `?institution_id=${institution.id}` : ''
    }`);
    this.component = AppRoot.getRouteInfo(route).getComponent();
  }

  renderMenuItems() {
    let currentHeader;
    return AppRoot.getRoutes()
      .filter((routeInfo) => routeInfo.menu
        && AppRoot.getPermissionsHelper().hasRole(routeInfo.access))
      .map((routeInfo) => {
        const comps = [
          (routeInfo.menu.header !== currentHeader ? (
            <ion-item-divider color="light" class="ion-no-padding">
              <ion-label class="bold-text" color="medium">
                {routeInfo.menu.header}
              </ion-label>
            </ion-item-divider>
          ) : null),
          <ion-item button={true} color={RouteService.path().match(routeInfo.url) ? 'medium' : 'light'}
                    onClick={() => this.onMenuItemClicked(routeInfo.url)}>
            <ion-icon slot="start" name={routeInfo.menu.icon}/>
            <ion-label>{routeInfo.menu.name}</ion-label>
          </ion-item>
        ];
        currentHeader = routeInfo.menu.header;
        return comps;
      });
  }

  render() {
    return [
      <ion-split-pane when="md" contentId="menu-content">
        <ion-menu menuId="menu" contentId="menu-content">
          <ion-header>
            <ion-toolbar color="primary">
              <ion-title>VisitDays Clone - Admin</ion-title>
            </ion-toolbar>
          </ion-header>
          <ion-content color="light">
            <ion-list lines="none" style={{'background': 'var(--ion-color-light)'}}
                      class="ion-padding">
              {this.renderMenuItems()}
            </ion-list>
          </ion-content>
        </ion-menu>
        <div id="menu-content">
          <app-header/>
          {this.component}
        </div>
      </ion-split-pane>
    ]
  }
}
