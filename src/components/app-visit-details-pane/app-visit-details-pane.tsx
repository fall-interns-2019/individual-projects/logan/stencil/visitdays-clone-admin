import {Component, h} from '@stencil/core';

@Component({
  tag: 'app-visit-details-pane',
  styleUrl: 'app-visit-details-pane.css'
})
export class AppVisitDetailsPane {

  render() {
    return [
      <ion-card style={{'--background': 'white'}}>
        <ion-item lines="none">
          <ion-icon name="clipboard" slot="start"/>
          Campus Tour
        </ion-item>
        <ion-item lines="none">
          <ion-icon name="calendar" slot="start"/>
          September 19, 2019
        </ion-item>
        <ion-item lines="none">
          <ion-icon name="time" slot="start"/>
          11:00am - 12:00pm
        </ion-item>
        <ion-item lines="none">
          <ion-icon name="pin" slot="start"/>
          Admissions Office
        </ion-item>
        <ion-item lines="none">
          <ion-icon name="people" slot="start"/>
          Sam and Erica
        </ion-item>
        <ion-label class="ion-padding ion-form-text" color="dark" text-wrap>
          Come visit our campus!
        </ion-label>
      </ion-card>
    ];
  }
}
