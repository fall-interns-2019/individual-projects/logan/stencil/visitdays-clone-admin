import {Component, h} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";
import {SessionService} from "../../services/session.service";
import {format} from 'date-fns';

@Component({
  tag: 'app-dashboard-today',
  styleUrl: 'app-dashboard-today.css'
})
export class AppDashboardToday {

  events: any[];

  async componentWillLoad() {
    let {data} = await AppRoot.getVisitTypeHelper().requestAll(
      `institution_id=${SessionService.get().institution_id}`
    );
    if (!data) return;
    // times_json: [{id, start_time, end_time, days: {"Monday": true, "Wednesday": true]}]
    const today = new Date();
    const day_name = format(today, 'EEEE');
    let events = [];
    data.forEach((visit_type) => {
      events = events.concat(
        JSON.parse(visit_type.times_json)
          .filter((occurrence) => {
            return Object.keys(occurrence.days).includes(day_name);
          })
          .map((occurrence) => {
            return {
              visit_type: {...visit_type},
              occurrence: {...occurrence}
            }
          })
      );
    });
    this.events = events;
  }

  renderEvents() {
    return this.events.map((event) => {
      return (
        <ion-item lines="none">
          <ion-label>
            <p>
              <b style={{'font-size': '1.2em'}}>
                {format(new Date(event.occurrence.start_time), 'p') + ' - ' +
                format(new Date(event.occurrence.end_time), 'p')}
              </b>
            </p>
          </ion-label>
          <ion-label class="ion-text-center">
            {event.visit_type.name}
          </ion-label>
          <ion-label class="ion-text-right">
            <p>
              {event.visit_type.location}
            </p>
          </ion-label>
        </ion-item>
      )
    });
  }

  render() {
    return [
      <ion-list>
        {this.renderEvents()}
      </ion-list>
    ];
  }
}
