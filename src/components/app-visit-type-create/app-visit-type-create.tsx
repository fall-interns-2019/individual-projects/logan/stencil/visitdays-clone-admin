import {Component, Element, h, Prop, State} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";
import $ from 'jquery';
import {modalController} from "@ionic/core";
import {SessionService} from "../../services/session.service";

@Component({
  tag: 'app-visit-type-create',
  styleUrl: 'app-visit-type-create.css'
})
export class AppVisitTypeCreate {

  @Element() el: HTMLElement;

  @Prop() visit_type: any;

  @State() occurrences: any[] = [];

  private formInputs = {};
  private form: HTMLFormElement;
  private debounce = false;
  private defaultOccurrence = () => {
    return {id: Math.random(), start_time: null, end_time: null, days: {}}
  };

  componentWillLoad() {
    if(this.visit_type) {
      this.occurrences = JSON.parse(this.visit_type.times_json);
    }
  }

  async onCancelClicked() {
    await modalController.dismiss();
  }

  async onAddVisitTypeClicked() {
    if (this.debounce || !this.form.reportValidity()) {
      return;
    }

    this.debounce = true;

    const name = this.formInputs['input-name'];
    const date_start = this.formInputs['input-date-start'];
    const date_end = this.formInputs['input-date-end'];
    const location = this.formInputs['input-location'];
    const times_json = JSON.stringify(this.occurrences);

    const button = $('#button-add')[0] as HTMLIonButtonElement;
    button.disabled = true;

    const result = await AppRoot.getVisitTypeHelper().upsert(
      this.visit_type ? this.visit_type.id : null,
      {
        institution_id: SessionService.get().institution_id,
        name, date_start, date_end, times_json, location
      })
      .catch((err) => {
        AppRoot.showNotification(err, 'danger');
      });

    this.debounce = false;
    button.disabled = false;

    if (!result) return;

    return modalController.dismiss(result.data)
  }

  private handleFormInputChanged(event) {
    this.formInputs[event.target.id] = event.detail.value;
  }

  private renderDayButtons(occurrence) {
    return (
        ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'].map((day, index) => {
          return (
            <ion-button class={index === 0 ? 'ion-margin-start' : null}
                        color={occurrence.days[day] ? 'primary' : 'light'}
                        onClick={() => {
                          const updated = [...this.occurrences];
                          const index = updated.findIndex((info) => info.id === occurrence.id);
                          updated[index].days[day] = !updated[index].days[day];
                          this.occurrences = updated;
                        }}>
              {day.substr(0, 1)}
            </ion-button>
          )
        })
    )
  }

  private renderOccurrences() {
    return this.occurrences.map((info, index) => {
      return (
        <ion-item color="light" lines="full" class="ion-form-text ion-padding-horizontal">
          <ion-datetime color="medium" display-format="h:mm A" picker-format="h:mm A"
                        placeholder={!info.start_time ? 'Start' : null}
                        value={info.start_time}
                        onIonChange={(evt) => {
                          this.occurrences[index].start_time = evt.detail.value;
                        }}/>
          <ion-datetime color="medium" display-format="h:mm A" picker-format="h:mm A"
                        placeholder={!info.end_time ? 'End' : null}
                        value={info.end_time}
                        onIonChange={(evt) => {
                          this.occurrences[index].end_time = evt.detail.value;
                        }}/>
          {this.renderDayButtons(info)}
          {index !== null ? (
            <ion-button slot="end" color="danger" fill="clear" size="default"
                        onClick={() => {
                          const updated = [...this.occurrences];
                          updated.splice(index);
                          this.occurrences = updated;
                        }}>
              <ion-icon slot="icon-only" name="trash"/>
            </ion-button>
          ) : null}
        </ion-item>
      )
    });
  }

  private addOccurrence() {
    this.occurrences = [...this.occurrences, this.defaultOccurrence()]
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-title>{this.visit_type ? 'Edit' : 'Create'} Visit Type</ion-title>
          <ion-buttons slot="primary">
            <ion-button color="primary" onClick={() => this.onCancelClicked()}>Cancel</ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-content color="light">
        <ion-card style={{'--background': '#ffffff'}}>
          <ion-card-content class="ion-no-padding">
            <form ref={(el) => this.form = el}>
              <ion-list lines="none">
                <ion-item color="light" lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Name</ion-label>
                  <ion-input id="input-name" required type="text" color="medium"
                             placeholder={
                               !this.visit_type ? "e.g., Advising Presentation" : null
                             }
                             value={this.visit_type ? this.visit_type.name : null}
                             onIonChange={(evt) => this.handleFormInputChanged(evt)}/>
                </ion-item>
                <ion-item color="light" lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Start Date</ion-label>
                  <ion-datetime id="input-date-start" color="medium" display-format="MMM DD, YYYY"
                                min={new Date().toISOString()}
                                max={(() => {
                                  const date = new Date();
                                  date.setFullYear(date.getFullYear() + 1);
                                  return date.toISOString();
                                })()}
                                placeholder={!this.visit_type ? 'Select Date' : null}
                                value={this.visit_type ? this.visit_type.date_start : null}
                                onIonChange={(evt) => this.handleFormInputChanged(evt)}/>
                </ion-item>
                <ion-item color="light" lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">End Date</ion-label>
                  <ion-datetime id="input-date-end" color="medium" display-format="MMM DD, YYYY"
                                min={new Date().toISOString()}
                                max={(() => {
                                  const date = new Date();
                                  date.setFullYear(date.getFullYear() + 1);
                                  return date.toISOString();
                                })()}
                                placeholder={!this.visit_type ? 'Select Date' : null}
                                value={this.visit_type ? this.visit_type.date_end : null}
                                onIonChange={(evt) => this.handleFormInputChanged(evt)}/>
                </ion-item>
                <ion-item color="light" lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Location</ion-label>
                  <ion-input id="input-location" required type="text" color="medium"
                             placeholder={
                               !this.visit_type ? "e.g., Advising Office" : null
                             }
                             value={this.visit_type ? this.visit_type.location : null}
                             onIonChange={(evt) => this.handleFormInputChanged(evt)}/>
                </ion-item>
                <ion-item-divider>Occurrences</ion-item-divider>
                {this.renderOccurrences()}
                <ion-button class="ion-padding-horizontal" color="primary"
                            onClick={() => this.addOccurrence()}>
                  Add
                </ion-button>
              </ion-list>
            </form>
            <ion-button id="button-add" class="ion-padding center-horiz half-width"
                        color="primary"
                        onClick={() => this.onAddVisitTypeClicked()}>
              {this.visit_type ? 'Edit' : 'Create'} Visit Type
            </ion-button>
          </ion-card-content>
        </ion-card>
      </ion-content>
    ];
  }
}
