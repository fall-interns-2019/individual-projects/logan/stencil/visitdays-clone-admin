import {Component, h} from '@stencil/core';
import {getCSSValue} from "../../helpers/utils";

@Component({
  tag: 'app-registrant-item',
  styleUrl: 'app-registrant-item.css'
})
export class AppRegistrantItem {

  render() {
    return [
      <ion-item lines="none">
        <ion-avatar slot="start">
          <img src={`https://ui-avatars.com/api/?name=Sam+Birk&rounded=true
          &background=${getCSSValue('--ion-color-primary')}&color=ffffff`}/>
        </ion-avatar>
        <ion-label slot="start">
          <p>
            <label class="bold-text">Sam Birk</label>
            <br/>
            1 Guest
          </p>
        </ion-label>
        <ion-icon slot="start" name="checkmark"/>
        <ion-button size="default">
          <ion-icon slot="start" name="checkmark"/>
          Check In
        </ion-button>
        <ion-button size="default" fill="outline" color="medium">
          <ion-icon slot="start" name="close-circle-outline"/>
          Not Here
        </ion-button>
        <ion-button size="default" fill="clear">
          <ion-icon slot="icon-only" name="more"/>
        </ion-button>
      </ion-item>
    ];
  }
}
