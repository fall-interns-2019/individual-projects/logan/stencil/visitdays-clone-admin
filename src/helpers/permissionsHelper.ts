import {AppRoot} from "../components/app-root/app-root";

export class PermissionsHelper {
  static roles = {
    none: { power: 0 },
    default: { power: 1 },
    member: { power: 2 },
    manager: { power: 3 },
    admin: { power: Infinity }
  };

  getDefaultRole() {
    return this.getRole('none');
  }

  getRole(roleName: string) {
    const role = PermissionsHelper.roles[roleName] || this.getDefaultRole();
    role.name = roleName.substr(0, 1).toUpperCase() + roleName.substr(1);
    return role;
  }

  getPower(roleName: string) {
    return this.getRole(roleName).power;
  }

  getUserRoles(user = AppRoot.getUser()) {
    if(!user) return [this.getDefaultRole()];

    return (user.data || user).roles.map((dbRole) => this.getRole(dbRole.name));
  }

  getHighestRole(user = AppRoot.getUser()) {
    if(!user) return this.getDefaultRole();

    return this.getUserRoles(user)
      .reduce((prevRole, role) => role.power > prevRole.power ? role : prevRole);
  }

  hasRole(roleName: string, exact = false) {
    return exact
      ? this.getUserRoles().some((role) => role.name === roleName)
      : this.getHighestRole().power >= this.getRole(roleName).power
  }
}
