import {ApiHelper} from "./apiHelper";

export class RestHelper extends ApiHelper {
  protected readonly modelUrl;

  constructor(modelUrl?) {
    super();
    this.modelUrl = modelUrl;
  }

  async requestAll(query?) {
    return ApiHelper.getApiHelper().request({
      method: 'GET',
      url: ApiHelper.API_URL + this.modelUrl + '?' +  (query || ''),
      headers: ApiHelper.getApiHelper().getRelevantHeaders()
    })
  }

  async get(id) {
    return ApiHelper.getApiHelper().request({
      method: 'GET',
      url: ApiHelper.API_URL + `${this.modelUrl}/${id}`,
      headers: ApiHelper.getApiHelper().getRelevantHeaders()
    })
  }

  async create(data) {
    return ApiHelper.getApiHelper().request({
      method: 'POST',
      url: ApiHelper.API_URL + this.modelUrl,
      headers: ApiHelper.getApiHelper().getRelevantHeaders(),
      data
    })
  }

  async update(id, data) {
    return ApiHelper.getApiHelper().request({
      method: 'PUT',
      url: ApiHelper.API_URL + `${this.modelUrl}/${id}`,
      headers: ApiHelper.getApiHelper().getRelevantHeaders(),
      data
    })
  }

  async upsert(id = null, data) {
    if(id)
      return this.update(id, data);
    else
      return this.create(data);
  }

  async destroy(id) {
    return ApiHelper.getApiHelper().request({
      method: 'DELETE',
      url: ApiHelper.API_URL + `${this.modelUrl}/${id}`,
      headers: ApiHelper.getApiHelper().getRelevantHeaders()
    })
  }
}
