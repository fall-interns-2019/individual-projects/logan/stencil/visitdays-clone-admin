import {RestHelper} from "./restHelper";

export class UserHelper extends RestHelper {
  constructor() {
    super('users');
  }
}
