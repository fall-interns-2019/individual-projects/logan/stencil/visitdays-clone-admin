import {RestHelper} from "./restHelper";

export class VisitTypeHelper extends RestHelper {
  constructor() {
    super('visit_types');
  }
}
