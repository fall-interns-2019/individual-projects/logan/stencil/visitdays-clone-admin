export function clamp(num: number, lower: number, upper: number) {
  return Math.min(Math.max(num, lower), upper);
}

export function delay(ms) {
  return new Promise(function (resolve) {
    setTimeout(resolve, ms);
  });
}

export function getBase64FromFile(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

export function getCSSValue(property) {
  return getComputedStyle(document.body)
    .getPropertyValue(property)
    .replace('#', '')
    .trim();
}
