import axios, {AxiosInstance, AxiosResponse, Method} from 'axios';
import {delay} from "./utils";
import {AppRoot} from "../components/app-root/app-root";
import _ from 'underscore';

export class ApiHelper {
  static readonly API_URL = 'http://localhost:3000/';

  axiosInstance: AxiosInstance;
  user = {data: null, headers: null, signedIn: false};

  constructor() {
    this.axiosInstance = axios.create({
      baseURL: ApiHelper.API_URL,
      timeout: 1000,
      headers: {'Content-Type': 'application/json'}
    });
  }

  static getApiHelper() {
    return document.querySelector('app-root').ApiHelper;
  }

  async request({method = 'GET' as Method, url = ApiHelper.API_URL, data = null, headers = null} = {}) {
    const response: any = await this.axiosInstance.request({
      method, url, data, headers,
      validateStatus: () => true
    }).catch((error) => {
      console.error('axios:', error);
    });

    if (response && response.headers) {
      Object.keys(response.headers).forEach((name) => {
        if (['cache-control', 'content-type'].includes(name)) return;

        if(!this.user.headers)
          this.user.headers = {};

        if(this.user.headers[name] !== response.headers[name]) {
          this.user.headers[name] = response.headers[name];
          // console.log('user header', name, 'set to', response.headers[name]);
        }
      });
    }

    return response;
  }

  async saveUserCredentials() {
    if (!this.user.headers) return false;

    window.localStorage.setItem('userCredentials', JSON.stringify(this.user.headers));
    return true;
  }

  async loadUserCredentials() {
    const data = window.localStorage.getItem('userCredentials');
    if (!data) return false;

    this.user.headers = JSON.parse(data);
    return true;
  }

  async deleteUserCredentials() {
    window.localStorage.removeItem('userCredentials');
  }

  async waitForUser() {
    while (!this.user.signedIn) {
      await delay(20);
    }

    return this.user;
  }

  getUser() {
    if(!this.user.signedIn) return;

    return this.user;
  }

  getRelevantHeaders() {
    if (!this.user.headers) return;
    const {uid, client, 'access-token': token} = this.user.headers;
    return {uid, client, 'access-token': token}
  }

  async emailSignUp(props, extra) {
    const {email, password, password_confirmation} = props;

    const response: AxiosResponse = await this.request({
      method: 'POST',
      url: ApiHelper.API_URL + 'auth',
      data: _.extend({email, password, password_confirmation}, extra)
    });

    if (response.data.errors) {
      throw new Error(
        response.data.errors.full_messages ? response.data.errors.full_messages[0] : response.data.errors[0]
      );
    }

    this.user.data = response.data;
    this.user.signedIn = true;

    this.saveUserCredentials();

    console.log('updated user:', this.user);
    return true;
  }

  async emailSignIn(props) {
    const {email, password} = props;

    const response: AxiosResponse = await this.request({
      method: 'POST',
      url: ApiHelper.API_URL + 'auth/sign_in',
      data: {email, password}
    });

    if (response.data.error || response.data.errors) {
      throw new Error(response.data.error || response.data.errors[0]);
    }

    this.user.data = response.data;
    this.user.signedIn = true;

    this.saveUserCredentials();

    console.log('updated user:', this.user);
    return true;
  }

  async signOut() {
    console.log('signing out: current user:', this.user);

    if (!this.user.headers) return false;

    const {uid, client, 'access-token': token} = this.user.headers;

    const response: AxiosResponse = await this.request({
      method: 'DELETE',
      url: ApiHelper.API_URL + 'auth/sign_out',
      headers: {uid, client, 'access-token': token}
    });

    if (response.data.error || response.data.errors) {
      throw new Error(response.data.error || response.data.errors[0]);
    }

    this.user.data = this.user.headers = null;
    this.user.signedIn = false;
    this.deleteUserCredentials();
    return true;
  }

  async updateUser(data) {
    if (!this.user.headers) return false;

    const {uid, client, 'access-token': token} = this.user.headers;

    const response: AxiosResponse = await this.request({
      method: 'PUT',
      url: ApiHelper.API_URL + 'auth',
      headers: {uid, client, 'access-token': token},
      data
    });

    if (response.data.errors) {
      throw new Error(
        response.data.errors.full_messages ? response.data.errors.full_messages[0] : response.data.errors[0]
      );
    }

    console.log('response:', response);

    this.user.data = response.data;
    this.user.signedIn = true;

    this.saveUserCredentials();

    console.log('updated user:', this.user);
    return true;
  }

  async validateUser() {
    this.loadUserCredentials();

    if (!this.user.headers) return false;

    const {uid, client, 'access-token': token} = this.user.headers;

    const response: AxiosResponse = await this.request({
      method: 'GET',
      url: ApiHelper.API_URL + 'auth/validate_token',
      headers: {uid, client, 'access-token': token}
    });

    if (response.data.errors) {
      this.user.data = this.user.headers = null;
      this.user.signedIn = false;
      this.deleteUserCredentials();

      throw new Error(response.data.errors[0]);
    }

    this.user.data = response.data;
    this.user.signedIn = true;
    this.saveUserCredentials();

    const current = await AppRoot.getCurrentInstitution();
    if(current && current.id !== this.user.data.institution_id) {
      AppRoot.getInstitutionHelper().setCurrent(this.user.data.institution);
    }

    return true;
  }

  async verifyUser() {
    let success = await AppRoot.getApiHelper().validateUser()
      .catch((err) => {
        console.log('validateUser error:', err);
      });

    if (!success) return false;

    console.log('validateUser: user is valid:', AppRoot.getUser());

    return success;
  }
}
