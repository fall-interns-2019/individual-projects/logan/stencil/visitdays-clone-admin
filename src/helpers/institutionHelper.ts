import {RestHelper} from "./restHelper";
import {SessionService} from "../services/session.service";

export class InstitutionHelper extends RestHelper {
  private _currentInstitution: any;

  async getCurrent() {
    if (!this._currentInstitution && SessionService.get().institution_id) {
      this.setCurrent((await this.get(SessionService.get().institution_id)).data);
      return this._currentInstitution;
    }
    return this._currentInstitution;
  }

  setCurrent(value: any) {
    this._currentInstitution = value;
    SessionService.update({institution_id: value ? value.id : null})
  }

  constructor() {
    super('institutions');
  }
}
