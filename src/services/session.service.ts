export class SessionService {
  static get(): {institution_id: number} {
    const cached = JSON.parse(localStorage.getItem('session')) || {};
    return {
      institution_id: cached.institution_id,
    };
  }

  static set(data) {
    localStorage.setItem('session', JSON.stringify(data));
  }

  static update(hash) {
    const updated = SessionService.get();
    Object.keys(hash).forEach((key) => {
      updated[key] = hash[key];
    });
    SessionService.set(updated);
  }

  static clear() {
    localStorage.removeItem('session');
  }
}
